from __future__ import annotations

from collections import Callable
from pathlib import Path
from typing import Dict

import numpy as np
import pyqtgraph as pg
from pyqtgraph.Qt import QtCore, QtWidgets
from qtextras import (
    EasyWidget,
    OptionsDict,
    ParameterEditor,
    attemptFileLoad,
    bindInteractorOptions as bind,
    safeSpawnDevConsole,
)
from s3a.plugins.file import ProjectData
from s3a.processing import ImagePipeline

from xrayrecon.cubecorrections.rotation import RotateWindow

from .helpers import FRPgParamDelegate
from .layermodel import LayerModel
from .layerview import LayerView
from .view3d import PCBViewer3D

pg.setConfigOption("imageAxisOrder", "row-major")

# def find_circles(image: np.ndarray, fgVerts, radius=10, param2='test'):


def find_circles(func: Callable, image=None):

    # out = np.zeros(image.shape[:2], 'uint8')
    # for vert in fgVerts:
    #   cv.circle(out, tuple(vert), radius, 1, -1)
    # return ProcessIO(image=out > 0)

    def findCirclesProcessor():
        proc = ImagePipeline(name=func.__name__)
        proc.addStage(func)
        return proc

    # procCtor =  ImageProcess.fromFunction(find_circles)
    # PRJ_SINGLETON.imgProcClctn.addProcessCtor(findCirclesProcessor)


class MainWin(QtWidgets.QMainWindow):
    def __init__(
        self,
        parent: QtWidgets.QWidget = None,
        layerInfo: Dict[str, list] = None,
        dataCube=None,
    ):
        super().__init__(parent)
        self.toolsEditor = ParameterEditor(name="Tools")

        from .algorithms import inputImage

        find_circles(inputImage)

        self.model = LayerModel(dataCube=dataCube, layerInfo=layerInfo)
        # Reassign dataCube in case it was modified
        self.dataCube = self.model.dataCube
        self.viewer = LayerView(self.model)
        self.viewer_3d = PCBViewer3D(dataCube)
        # self.model.sigCubeChanged.connect(
        #     lambda: self.viewer_3d.setDataCube(self.model.dataCube)
        # )

        self.tbl = QtWidgets.QTableView()
        self.tbl.setModel(self.model.tblModel)
        self.tbl.selectionModel().selectionChanged.connect(self.updateTblViews)
        pgDict = dict(type="list", values={"True": True, "False": False})
        self.tbl.setItemDelegateForColumn(0, FRPgParamDelegate(pgDict))
        self.tbl.setSelectionBehavior(self.tbl.SelectionBehavior.SelectRows)

        self.plt = pg.PlotWidget()
        self.viewer.itemParent = self.plt
        for item in self.viewer.items:
            self.plt.addItem(item)
        vb = self.plt.getViewBox()
        vb.setAspectLocked(True)

        self.toolsEditor.registerFunction(self.loadCubeFromFile)
        self.toolsEditor.registerFunction(
            self.fixRotation, name="Interactive Rotation Fix"
        )
        self.toolsEditor.registerFunction(self.showDevConsole, name="Dev Console")
        self.toolsEditor.registerFunction(
            self.createS3AProject, name="Create S3A Project"
        )
        highlightProc = self.toolsEditor.registerFunction(
            self.highlight3DSlices, name="Show Selection in 3D"
        )
        liveUpdateParameter = self.toolsEditor.registerParameter(
            OptionsDict("Live Update 3D", False)
        )
        # self.toolsEditor.registerFunc(self.viewer_3d.resetZoom())
        def maybeUpdate():
            if liveUpdateParameter.value():
                self.highlight3DSlices(highlightProc.parameterCache["downsampleFactor"])

        self.tbl.selectionModel().selectionChanged.connect(maybeUpdate)

        def maybeShowMetric():
            if self.model.metricInfo is not self.model.NO_METRIC:
                self.model.metricInfo.show()
            else:
                QtWidgets.QMessageBox.information(
                    self, "No Metric", "No metric to show."
                )

        self.toolsEditor.registerFunction(
            maybeShowMetric, name="Show Layer Slice Metrics"
        )
        self._buildUi()

    def _buildUi(self):
        menu = QtWidgets.QMenuBar(self)
        self.setMenuBar(menu)

        children = EasyWidget(
            [[self.toolsEditor, self.tbl], self.viewer_3d, self.plt],
            useSplitter=True,
            layout="H",
        )
        # Extra layer of nesting allows margin to look a bit better
        EasyWidget.buildMainWindow([children], window=self)
        self.setWindowTitle("X-ray Reconstruction")

    @bind(file=dict(type="file", nameFilter="*.npy", value=""))
    def loadCubeFromFile(self, file: str | Path):
        if not file:
            return
        data = np.load(file)
        self.model.setCube(data)

    @bind(location=dict(type="file", fileMode="Directory", value=""))
    @bind(alert=dict(value=True, ignore=True))
    def createS3AProject(self, location: str | Path = None, alert=False):
        if not location:
            return
        here = Path(__file__).resolve().parent
        prj = ProjectData.create(
            name=location, config=attemptFileLoad(here.parent / "projcfg.s3aprj")
        )
        layerDf = self.model.layerDf
        for ii, name in enumerate(layerDf["Layer Name"]):
            imgName = prj.imagesPath / f"{name}.png"
            if imgName not in prj.images:
                selectedSlices = layerDf.loc[ii, "Slices"]
                img = self.model.dataCube[..., selectedSlices].mean(2).astype("uint8")
                prj.addImage(imgName, data=img, copyToProject=True, allowOverwrite=True)
        if alert:
            msgBox = QtWidgets.QMessageBox(self)
            msgBox.setWindowTitle("Project Created")
            msgBox.setText(
                f"<qt>Created S3A project. To make annotations, open it in S3A using "
                f"the terminal command<br><br>"
                f"<code>s3a-gui --project {prj.configPath}</code></qt>"
            )
            msgBox.setTextInteractionFlags(
                QtCore.Qt.TextInteractionFlag.TextSelectableByMouse
            )
            msgBox.setModal(True)
            msgBox.exec()

    def fixRotation(self):
        win = RotateWindow(self.model.dataCube)
        win.show()
        win.sigChangesAccepted.connect(lambda: self.model.setCube(win.cube))

    def showDevConsole(self):
        safeSpawnDevConsole(self)

    def updateTblViews(
        self, selected: QtCore.QItemSelection, deselected: QtCore.QItemSelection
    ):
        selRows = [sel.row() for sel in selected.indexes()]
        deselRows = [sel.row() for sel in deselected.indexes()]
        selRows = np.unique(selRows)
        deselRows = np.unique(deselRows)
        self.model.tblModel.dataDf.loc[deselRows, "View"] = False
        self.model.tblModel.dataDf.loc[selRows, "View"] = True
        self.model.tblModel.sigDataChanged.emit()

    @bind(downsampleFactor=dict(limits=(1, 10)), transparency=dict(limits=(0, None)))
    def highlight3DSlices(self, downsampleFactor=3, transparencyFactor=3.0):
        """
        Parameters
        ----------
        downsampleFactor
            How much to downsample the 3D view by. 1 means no downsampling.
        transparencyFactor
            How much to multiply the transparency of the slices by. 1 means no scaling
            is applied.
        """
        selectedRows = self.model.layerDf["View"]
        if np.any(selectedRows):
            selectedSlices = np.concatenate(
                self.model.layerDf.loc[selectedRows, "Slices"].values
            )
        else:
            selectedSlices = None
        self.viewer_3d.setDataCube(
            self.model.dataCube, selectedSlices, downsampleFactor, transparencyFactor
        )
