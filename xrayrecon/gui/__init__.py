import pyqtgraph as pg

from .app import MainWin

pg.setConfigOption("imageAxisOrder", "row-major")
