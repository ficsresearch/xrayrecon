import numpy as np
import pyqtgraph as pg
from fire import Fire
from pyqtgraph.Qt import QtCore

from xrayrecon.gui import MainWin
from xrayrecon.gui.helpers import phantomSpheres


def main(datafile: str = None):
    if datafile is None:
        # Use dummy data
        np.random.seed(42)
        dataCube = phantomSpheres(n=256)
    else:
        dataCube = np.load(datafile)

    app = pg.mkQApp()
    layerInfo = None
    win = MainWin(None, layerInfo, dataCube)
    QtCore.QTimer.singleShot(0, win.showMaximized)
    app.exec_()


def main_script():
    Fire(main)


if __name__ == "__main__":
    main_script()
